SHELL := /bin/bash

html:
	time jupyter-book build book

pdf:
	jupyter-book build book --builder pdflatex

clean:
	rm -rf book/_build

format:
	mdformat README.md book
	@# black book
