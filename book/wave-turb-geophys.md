# Waves & turbulence in geophysical flows

```{image} ./atmo_wake_waves.jpg
---
alt: Atmospheric wave wakes (NASA)
width: 40%
align: right
---
```

Geophysical flows are affected by planetary rotation and by gravity. Because of
these two effects, waves can propagate in fluids either inertial waves due to the
Coriolis force or internal gravity waves if the fluid is stratified in density.
These waves are fundamental ingredients of the dynamics of oceans or the
atmosphere. When the waves become nonlinear they contribute to turbulence in
addition to vortices. Indeed, large amplitude waves can exchange energy through
nonlinearity and thus contribute to the famous cascade of energy in scale which is
the fundamental characteristics of turbulence. Very large amplitude waves can also
overturn and generate directly small-scale turbulence. In oceans, the contribution
of internal waves to turbulence has a major impact on the global energy budget by
transferring energy to small scales at which it is dissipated either through
viscous dissipation of through irreversible mixing of the stratification. This
mixing has a major impact on maintaining the large-scale oceanic circulation and
thus must be considered in global modeling (MacKinnon et al. Bull. AMS 2017).
However, as this contribution occurs are small scales compared to the size of the
planet, it must be parametrized accurately models as numerical simulations cannot
resolve these scales. This motivates fundamental studies to understand the
detailed dynamical regimes and the statistical properties of geophysical
turbulence with waves.
