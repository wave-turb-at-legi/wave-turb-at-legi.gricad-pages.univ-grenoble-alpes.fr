# Two postdoc positions

One post-doc position will be focused on experimental investigations and the other
position on numerical aspects. One of the positions can start as soon as possible
(within the administrative constraints). The second position should start after
September 1st 2023. Each position is at least 2 years and one position can be
extended to 3 years (on condition that it starts early enough). The project is
hosted on the campus of Université Grenoble Alpes, on the premises of [LEGI] which
is a fluid mechanics laboratory will very diverse research interests and with a
strong experimental component.

The postdocs will participate to the Simons Collaboration Annual Meeting that is held at the Simons Foundation premises in Manhattan in early december each year as well as to Summer schools organized by the collaboration. 

```{important}

Applicants must contact Nicolas Mordant
(nicolas.mordant@univ-grenoble-alpes.fr) or Pierre Augier
(pierre.augier@univ-grenoble-alpes.fr) for further information and application.

```

## Experimental position

```{image} ./coriolis.jpg
---
alt: Picture of the Coriolis platform.
width: 400px
align: right
---
```

Previous investigations have studied stratified turbulence forced by waves in the
CORIOLIS facility {cite:p}`Rodda2022Experimental`. We observed that a regime of
weak wave turbulence develops at large scales that evolves into strongly nonlinear
turbulence at small scales. Further developments may concern:

- analyses of the existing database notably the case of rotating and stratified
  turbulence with comparison with the purely stratified case. Additional
  experiments can/will be conducted in 2024-2025 (the schedule is dependent on other projets on the Coriolis facility).
- investigation of Lagrangian measurements of particle trajectories in rotating &
  stratified turbulence. Analysis of the preliminary experiments and setup of new experiments.
- investigations of mixing either through density profiles or through the
  development of PIV-LIF in the CORIOLIS facility. Preliminary experiments would be done in a smaller scale experiment to investigate the feasibility of such experiments in the Coriolis facility.
- investigation of overturning waves and the associated space-time intermittency.
- interaction with the other post-doc for comparison of statistics of experiments and numerical simulations.
- collaboration with Joel Someria and coworkers on the occurrence of waves in convection in stratified fluid in the Coriolis facility.

We welcome suggestions of other axes of research by the applicant. 

The project
being experimental, the applicant must have developed an expertise in
experimental fluid dynamics with experience of the use of Particle Image
Velocimetry and/or Particle Tracking Velocimetry. He/She must be familiar with
standard statistical data processing (Fourier spectra, correlations, PDF…).
Skills in Python programming will be useful. This position will be supervised by
Nicolas Mordant.

```{image} ./overturn_wave.png
---
alt: overturning internal wave
width: 95%
align: center
---
```

## Numerical position

We will investigate numerically internal wave turbulence in close collaboration
with other PIs working on experiments and theories. In the current Simons
collaboration on wave turbulence, Nicolas Mordant and Pierre-Philippe Cortet were
able to obtain experimentally different regimes of internal wave turbulence forced
by waves in the Coriolis facility (LEGI, Grenoble, France) and in a smaller
rotating platform (FAST, Orsay, France). In the meantime, we developed a
pseudo-spectra [Fluidsim] solver to simulate with a very good accuracy internal
gravity wave turbulence with on one hand forcing schemes mimicking the experiments
and on the other hand idealized forcing schemes computed in Fourier space. With
both types of simulations, we can explore parameters than are not accessible in
experiments and gather data which cannot be measured in the experiments. In
particular, our code can compute spatiotemporal spectra and spectral energy
budget, which are two very important tools to better understand the dynamics.

We now want to focus on questions related to the physical explanation of the
oceanic and atmospheric spectra. What will be the effect of rotation on the forced
internal wave turbulence regimes observed experimentally and numerically? How the
mixing coefficient evolves with parameters in the different wave turbulence
regimes? Which mechanisms dominate the mixing? We also like to study the decay in
the presence of stratification and rotation of synthetic fields reproducing the
oceanic and atmospheric spectra. These investigations will be done in direct
collaboration with other PIs: Nicolas Mordant and Pierre-Philippe Cortet who are
doing experiments on stratified turbulence, Giorgio Krstulovic who also
investigate this problem with Direct Numerical Simulations and Sergey Nazarenko
who is more focused on theoretical aspects.

Some years ago, we derived an exact law about the way the energy cascades through
scales in stratified turbulence (Augier, Galtier, Billant, 2012). We would like to
compute from numerical results the terms involved in this law to finally get a
confirmation of these predictions. This would allow us to better understand the
anisotropic cascade and to investigate the universality of strongly stratified
turbulence. We could then try to apply this method to different types of wave
turbulence influenced by other effects, like in particular rotation.

This position will be supervised by Pierre Augier.

[fluidsim]: https://fluidsim.readthedocs.io
[legi]: http://www.legi.grenoble-inp.fr
