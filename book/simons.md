# Collaboration on wave turbulence supported by the Simons Foundation

```{image} ./simons-foundation.png
---
alt: The Simons Foundation
width: 400px
align: right
---
```

The Simons Foundation supports an international and interdisciplinary
[collaboration on wave turbulence](https://cims.nyu.edu/wave-turbulence/) directed
by [Prof. Jalal Shatah] from New York University that involves 20 PIs from the US,
France and Italy and including N. Mordant and P. Augier from [LEGI]. This funding
runs until 2026. The funding supports two post-doctoral positions in [LEGI] for 2-
and 3-years resp. The scientific program of the collaboration is to investigate
various aspects of wave turbulence either from the mathematical or the physical
point of view. The centerpiece of the investigations in the collaboration is the
Weak Turbulence Theory (WTT), which aims at describing the temporal evolution of
the statistical properties {cite:p}`Nazarenko2011Wave`. The application of WTT to
the case of internal gravity waves remains the object of investigations either at
the theoretical and experimental/numerical level. Furthermore it is well known
that in realistic cases of stratified turbulence, weak wave turbulence is only
part of the problem. Wave coexist with vortices and all these degrees of freedom
are coupled by nonlinearity that can be weak or strong. One major open question is
the interplay of such weak wave turbulence and strong turbulence that can develop
either at very large scale in which case it is strongly anisotropic or at the
smallest scales (possibly isotropic). Our laboratory (Laboratoire des Ecoulements
Géophysiques et Industriels, [LEGI]) is a privileged place for such investigations
of stratified turbulence due to a long-standing expertise in the field. On the
experimental point of view, [LEGI] host a unique facility, the CORIOLIS facility,
dedicated to the experimental investigations of geophysical flows. It consists in
a 13m diameter, 1m-deep rotating tank equipped with a specific hydraulic scheme to
generate arbitrary salt stratification profiles. Many sorts of measurements can be
implemented such as PIV (Particle Image Velocimetry), ultrasonic velocimetry or
density profiles. Numerical simulations will also be performed using various
numerical codes such as the open source solver [Fluidsim].

One post-doc position will be focused on experimental investigations and the other
position on numerical aspects. One of the positions can start as soon as possible
(within the administrative constraints). The second position should start after
September 1st 2023. Each position is at least 2 years and one position can be
extended to 3 years (on condition that it starts early enough). The project is
hosted on the campus of Université Grenoble Alpes, on the premises of [LEGI] which
is a fluid mechanics laboratory will very diverse research interests and with a
strong experimental component.

```{important}

Applicants must contact Nicolas Mordant
(nicolas.mordant@univ-grenoble-alpes.fr) or Pierre Augier
(pierre.augier@univ-grenoble-alpes.fr) for further information and application.

```

[fluidsim]: https://fluidsim.readthedocs.io
[legi]: http://www.legi.grenoble-inp.fr
[prof. jalal shatah]: https://math.nyu.edu/people/profiles/SHATAH_Jalal.html
