# Wave turbulence at LEGI

```{image} ./atmo_wake_waves.jpg
---
alt: Atmospheric wave wakes (NASA)
width: 50%
align: right
---
```

This website presents our activity on
[wave turbulence in geophysical flows](./wave-turb-geophys.md) at [LEGI]
(Laboratoire des Ecoulements Géophysiques et Industriels). Our laboratory has a
long-standing expertise in the fields of turbulence and waves. The activity on
wave turbulence started with the
[WATU ERC grant](http://nicolas.mordant.free.fr/watu.html) lead by Prof. Nicolas
Mordant. This activity continued with an
[international collaboration on wave turbulence](./simons.md) funded in 2019 by
the [Simons Foundation] and led by [Prof. Jalal Shatah] from the Courant Institute
of NYU. The support of this collaboration has just been renewed so it will
continue at least until 2026.

## Two postdoc positions opened

We seek for two doctors to join our group as postdocs funded by the
[Simons Foundation] through an international and interdisciplinary
[collaboration on wave turbulence](./simons.md). For the
[detailed description of these 2 to 3 years postdoc positions, click here](./postdocs.md).

[legi]: http://www.legi.grenoble-inp.fr
[prof. jalal shatah]: https://math.nyu.edu/people/profiles/SHATAH_Jalal.html
[simons foundation]: https://www.simonsfoundation.org/
