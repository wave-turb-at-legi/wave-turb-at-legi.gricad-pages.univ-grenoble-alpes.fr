# Wave Turbulence at LEGI

Source of the book https://wave-turb-at-legi.gricad-pages.univ-grenoble-alpes.fr/

The source can be edited online at
https://gricad-gitlab.univ-grenoble-alpes.fr/wave-turb-at-legi/wave-turb-at-legi.gricad-pages.univ-grenoble-alpes.fr

To build and edit locally on your computer, I would do something like (with
[Poetry](https://python-poetry.org/docs/#installation) and Mercurial+hg-git
installed)

```sh
hg clone git@gricad-gitlab.univ-grenoble-alpes.fr:wave-turb-at-legi/wave-turb-at-legi.gricad-pages.univ-grenoble-alpes.fr.git wave-turb-at-legi
cd wave-turb-at-legi
poetry install
poetry shell
# edit as needed
make
hg commit -m "An informative commit message"
hg push
```
